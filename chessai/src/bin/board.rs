use chessai::game::Game;
use chessai::players;

fn main() {
    let white = players::VitSkolmatt::new();
    let black = players::SvartSkolmatt::new();
    let mut game = Game::new(white, black);
    game.start();
}
