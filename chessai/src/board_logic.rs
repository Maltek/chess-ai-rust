use std::ops::{Index, IndexMut};

#[derive(Clone)]
pub struct Board {
    //Entries are enums of pieces first and then player
    //Maybe make this an Option<Piece> instead of Piece?
    pub board_vec: Vec<(Piece)>,
    //We need to keep track of last significant move
    // moves_since_LSM: i8,
    // if the king has moved matters for castling
    pub w_k_has_moved: bool,
    pub b_k_has_moved: bool,
    // The rooks cannot have moved for castling
    pub left_white_rook_has_moved: bool,
    pub right_white_rook_has_moved: bool,
    pub left_black_rook_has_moved: bool,
    pub right_black_rook_has_moved: bool,
    //The color of the color to move should be stored here
    pub color_to_move: Colors,
    //Add is_checked bool here for optimization
    pub is_checked: bool,
}

impl Index<&str> for Board {
    type Output = Piece;
    fn index(&self, pos: &str) -> &Self::Output {
        &self.board_vec[Board::sqr_to_idx(pos) as usize]
    }
}

impl IndexMut<&str> for Board {
    fn index_mut(&mut self, pos: &str) -> &mut Self::Output {
        &mut self.board_vec[Board::sqr_to_idx(pos) as usize]
    }
}

impl Board {
    pub fn new() -> Board {
        let mut new_board_vec: Vec<(Piece)> = vec![
            Piece::new(Pieces::Rook, Colors::White),
            Piece {
                piece: Pieces::Knight,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Bishop,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Queen,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::King,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Bishop,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Knight,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Rook,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::White,
            },
        ];
        let middle_of_board: Vec<(Piece)> = vec![
            Piece {
                piece: Pieces::Empty,
                color: Colors::None,
            };
            32
        ];
        let black_pieces: Vec<(Piece)> = vec![
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Pawn,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Rook,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Knight,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Bishop,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Queen,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::King,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Bishop,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Knight,
                color: Colors::Black,
            },
            Piece {
                piece: Pieces::Rook,
                color: Colors::Black,
            },
        ];
        new_board_vec.extend(middle_of_board);
        new_board_vec.extend(black_pieces);
        Board {
            board_vec: new_board_vec,
            w_k_has_moved: false,
            b_k_has_moved: false,
            left_black_rook_has_moved: false,
            right_black_rook_has_moved: false,
            left_white_rook_has_moved: false,
            right_white_rook_has_moved: false,
            color_to_move: Colors::White,
            is_checked: false,
        }
    }

    pub fn blank() -> Board {
        Board {
            board_vec: vec![
                Piece {
                    piece: Pieces::Empty,
                    color: Colors::None,
                };
                64
            ],
            b_k_has_moved: false,
            w_k_has_moved: false,
            left_black_rook_has_moved: false,
            right_black_rook_has_moved: false,
            left_white_rook_has_moved: false,
            right_white_rook_has_moved: false,
            color_to_move: Colors::White,
            is_checked: false,
        }
    }

    pub fn legal_move(&self, old_idx: i8, new_idx: i8) -> bool {
        if old_idx == new_idx {
            return false;
        }
        let piece_to_move: &Piece = &self.board_vec[old_idx as usize];
        if piece_to_move.color != self.color_to_move {
            return false;
        }
        piece_to_move.legal_move(&self, old_idx, new_idx)
    }

    pub fn legal_moves(&self) -> Vec<(i8, i8)> {
        let mut moves: Vec<(i8, i8)> = Vec::new();
        for value in 0..64 {
            moves.extend(
                (0..64)
                    .filter(|&x| &self.board_vec[x as usize].color == &self.color_to_move)
                    .map(|x| (x, value))
                    .filter(|&(old_idx, new_idx)| self.legal_move(old_idx, new_idx))
                    .collect::<Vec<(i8, i8)>>(),
            )
        }

        moves
    }

    pub fn sqr_to_idx(sqr: &str) -> i8 {
        let mut idx: i8 = 0;
        if sqr.len() == 2 {
            let column = sqr.chars().nth(0);
            match column {
                Some(_char) => match column.unwrap() {
                    'a' => idx += 0,
                    'b' => idx += 1,
                    'c' => idx += 2,
                    'd' => idx += 3,
                    'e' => idx += 4,
                    'f' => idx += 5,
                    'g' => idx += 6,
                    'h' => idx += 7,
                    _ => panic!(
                        "The first character needs to be a letter between a and h in the alphabet"
                    ),
                },
                _ => panic!(
                    "The first character needs to be a letter between a and h in the alphabet"
                ),
            }
            let ch = sqr.chars().nth(1).unwrap_or('9');
            let row = ch.to_digit(10).unwrap_or(9);
            idx += match row {
                (1...8) => ((row - 1) * 8) as i8,
                _ => panic!("The second character needs to be an integer between 1 and 8"),
            }
        } else {
            panic!("{} is not two characters long", sqr)
        }
        idx
    }
    //I need to add pawn promotion
    pub fn make_move(&self, old_idx: i8, new_idx: i8) -> Board {
        let mut new_board_vec = self.board_vec.clone();
        //Copy the information about the piece to be moved
        let piece = new_board_vec[old_idx as usize].clone();
        new_board_vec[old_idx as usize] = Piece {
            piece: Pieces::Empty,
            color: Colors::None,
        };
        new_board_vec[new_idx as usize] = piece;
        let w_k_has_moved = self.w_k_has_moved
            || self.board_vec[old_idx as usize]
            == Piece {
            piece: Pieces::King,
            color: Colors::White,
        };
        let b_k_has_moved = self.b_k_has_moved
            || self.board_vec[old_idx as usize]
            == Piece {
            piece: Pieces::King,
            color: Colors::Black,
        };
        let left_black_rook_has_moved =
            self.left_black_rook_has_moved || old_idx == Board::sqr_to_idx("a8");
        let right_black_rook_has_moved =
            self.right_black_rook_has_moved || old_idx == Board::sqr_to_idx("h8");
        let left_white_rook_has_moved =
            self.left_white_rook_has_moved || old_idx == Board::sqr_to_idx("a1");
        let right_white_rook_has_moved =
            self.right_white_rook_has_moved || old_idx == Board::sqr_to_idx("h1");
        let color_to_move = match self.color_to_move {
            Colors::White => Colors::Black,
            Colors::Black => Colors::White,
            _ => panic!("Either white or black needs to move"),
        };
        let mut new_board = Board {
            board_vec: new_board_vec,
            b_k_has_moved,
            w_k_has_moved,
            left_black_rook_has_moved,
            right_black_rook_has_moved,
            left_white_rook_has_moved,
            right_white_rook_has_moved,
            color_to_move,
            is_checked: false,
        };
        new_board.is_checked = new_board.calculate_check(&new_board.color_to_move);
        new_board
    }

    fn move_str_to_index(mv: &str) -> (i8, i8) {
        let squares = mv.split("-").collect::<Vec<_>>();
        if squares.len() != 2 {
            panic!()
        }
        let old_idx = Board::sqr_to_idx(squares[0]);
        let new_idx = Board::sqr_to_idx(squares[1]);
        (old_idx, new_idx)
    }

    pub fn make_str_move(&self, mv: &str) -> Board {
        let (old_idx, new_idx) = Board::move_str_to_index(mv);
        self.make_move(old_idx, new_idx)
    }
    //Mb optimize order of operations (check piece first then color)
    pub fn calculate_check(&self, color: &Colors) -> bool {
        let opposing_color = match color {
            Colors::Black => Colors::White,
            Colors::White => Colors::Black,
            _ => panic!(),
        };
        let mut king_idx: i8 = -1;
        for x in 0..64 {
            if &self.board_vec[x as usize].color == color
                && self.board_vec[x as usize].piece == Pieces::King
            {
                king_idx = x;
            }
        }
        (0..64)
            .filter(|&x| &self.board_vec[x as usize].color == &opposing_color)
            .any(|x| self.board_vec[x as usize].can_take(&self, x, king_idx))
    }

    pub fn status(&self) -> GameResult {
        let moves = self.legal_moves();

        if moves.is_empty() {
            if self.is_checked {
                return match self.color_to_move {
                    Colors::Black => GameResult::WhiteWin,
                    Colors::White => GameResult::BlackWin,
                    _ => panic!("")
                };
            } else {
                return GameResult::Draw;
            }
        }
        GameResult::Ongoing
    }
}

//An enumerator for color colors
#[derive(PartialEq, Clone)]
pub enum Colors {
    None,
    White,
    Black,
}

impl fmt::Display for Colors {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let result = match self {
            Colors::Black => "Black",
            Colors::White => "White",
            Colors::None => "None",
        };
        write!(f, "{}", result)
    }
}

//An enumerator for all the pieces. An empty tile is represented as a separate piece.
#[derive(PartialEq, Clone)]
pub enum Pieces {
    Empty,
    Rook,
    Knight,
    Bishop,
    Queen,
    King,
    Pawn,
}

#[derive(Clone, PartialEq)]
pub struct Piece {
    piece: Pieces,
    color: Colors,
}

impl Piece {
    pub fn new(piece: Pieces, color: Colors) -> Piece {
        Piece { piece, color }
    }

    /*TO-DO:
implement en passant
optimize (only check if is in check after checking if a piece can even move there?)

*/

    fn follows_pattern(&self, board: &Board, old_idx: i8, new_idx: i8) -> bool {
        match self.piece {
            Pieces::Pawn => match self.color {
                Colors::White => match new_idx - old_idx {
                    8 => {
                        if board.board_vec[new_idx as usize].piece == Pieces::Empty {
                            return true;
                        }
                    }
                    16 => {
                        if old_idx < 8 || old_idx > 15 {
                            return false;
                        }
                        if board.board_vec[new_idx as usize].piece == Pieces::Empty
                            && board.board_vec[(old_idx + 8) as usize].piece == Pieces::Empty
                        {
                            return true;
                        }
                    }
                    7 | 9 => {
                        if board.board_vec[new_idx as usize].color == Colors::Black
                            && !Piece::out_of_bounds_step(old_idx, new_idx)
                        {
                            return true;
                        }
                        return false;
                    }
                    _ => return false,
                },
                Colors::Black => match new_idx - old_idx {
                    -8 => {
                        if board.board_vec[new_idx as usize].piece == Pieces::Empty {
                            return true;
                        }
                    }
                    -16 => {
                        if old_idx < 48 || old_idx > 55 {
                            return false;
                        }
                        if board.board_vec[new_idx as usize].piece == Pieces::Empty
                            && board.board_vec[(old_idx - 8) as usize].piece == Pieces::Empty
                        {
                            return true;
                        }
                    }
                    -9 | -7 => {
                        if board.board_vec[new_idx as usize].color == Colors::White
                            && !Piece::out_of_bounds_step(old_idx, new_idx)
                        {
                            return true;
                        }
                        return false;
                    }
                    _ => return false,
                },
                _ => return false,
            },
            Pieces::Bishop => {
                let diff = new_idx - old_idx;
                let sign = match diff > 0 {
                    true => 1,
                    false => -1,
                };
                let steps;
                let direction;
                if (diff % 7) == 0 {
                    steps = diff.abs() / 7;
                    direction = sign * 7;
                } else if (diff % 9) == 0 {
                    steps = diff.abs() / 9;
                    direction = sign * 9;
                } else {
                    return false;
                }
                let mut prev_idx = old_idx;
                for _i in 1..steps {
                    let next_idx = prev_idx + direction;
                    if board.board_vec[next_idx as usize].color != Colors::None
                        || Piece::out_of_bounds_step(prev_idx, next_idx)
                    {
                        return false;
                    }
                    prev_idx = next_idx;
                }
                return !Piece::out_of_bounds_step(prev_idx, new_idx) && board.board_vec[new_idx as usize].color != board.board_vec[old_idx as usize].color;
            }
            Pieces::King => {
                let diff = new_idx - old_idx;
                return match diff {
                    //A king can move in every direction
                    8 | 9 | 1 | -7 | -8 | -9 | -1 | 7 => {
                        return !Piece::out_of_bounds_step(old_idx, new_idx);
                    }
                    //Under some circumstances we can castle
                    2 | -2 => match self.color {
                        Colors::White => {
                            if !board.w_k_has_moved && !board.is_checked {
                                if diff == 2 {
                                    return !board.right_white_rook_has_moved
                                        && !board
                                        .make_move(old_idx, old_idx + 1)
                                        .is_checked && Piece::not_blocked(board, old_idx, 1, 2);
                                }
                                return !board.left_white_rook_has_moved
                                    && !board
                                    .make_move(old_idx, old_idx - 1)
                                    .is_checked && Piece::not_blocked(board, old_idx, -1, 3);
                            }
                            return false;
                        }
                        Colors::Black => {
                            if !board.b_k_has_moved && !board.is_checked {
                                if diff == 2 {
                                    return !board.right_black_rook_has_moved
                                        && !board
                                        .make_move(old_idx, old_idx + 1)
                                        .is_checked && Piece::not_blocked(board, old_idx, 1, 2);
                                }
                                return !board.left_black_rook_has_moved
                                    && !board
                                    .make_move(old_idx, old_idx - 1)
                                    .is_checked && Piece::not_blocked(board, old_idx, -1, 3);
                            }
                            return false;
                        }
                        _ => return false,
                    },
                    _ => return false,
                };
            }
            Pieces::Rook => {
                let diff = new_idx - old_idx;
                let sign = match diff > 0 {
                    true => 1,
                    false => -1,
                };
                if new_idx / 8 == old_idx / 8 {
                    let steps = diff;
                    for i in 1..steps {
                        if board.board_vec[(old_idx + i * sign) as usize].color != Colors::None {
                            return false;
                        }
                    }
                    return true;
                } else if (diff % 8) == 0 {
                    let steps = diff / 8;
                    for i in 1..steps {
                        if board.board_vec[(old_idx + i * 8 * sign) as usize].color
                            != Colors::None
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }

            Pieces::Queen => {
                if Piece::new(Pieces::Bishop, self.color.clone())
                    .follows_pattern(&board, old_idx, new_idx)
                    || Piece::new(Pieces::Rook, self.color.clone())
                    .follows_pattern(&board, old_idx, new_idx)
                {
                    return true;
                }
            }
            Pieces::Knight => {
                let diff = new_idx - old_idx;
                match diff {
                    17 | 10 | -6 | -15 => match new_idx % 8 {
                        0 => return false,
                        1 => {
                            if diff == -6 || diff == 10 {
                                return false;
                            }
                        }
                        _ => return true,
                    },
                    15 | -17 | -10 | 6 => match new_idx % 8 {
                        7 => return false,
                        6 => {
                            if diff == 6 || diff == -10 {
                                return false;
                            }
                        }
                        _ => return true,
                    },
                    _ => return false,
                }
            }

            _ => return false,
        }
        false
    }

    fn can_take(&self, board: &Board, old_idx: i8, new_idx: i8) -> bool {
        match self.piece {
            Pieces::Pawn => {
                let diff = old_idx - new_idx;
                match self.color {
                    Colors::Black => {
                        if diff == -7 || diff == -9 {
                            !Piece::out_of_bounds_step(old_idx, new_idx)
                        } else {
                            false
                        }
                    }
                    Colors::White => {
                        if diff == 7 || diff == 9 {
                            !Piece::out_of_bounds_step(old_idx, new_idx)
                        } else {
                            false
                        }
                    }

                    _ => false,
                }
            }
            Pieces::King => match old_idx - new_idx {
                8 | 9 | 1 | -7 | -8 | -9 | -1 | 7 => {
                    return !Piece::out_of_bounds_step(old_idx, new_idx);
                }
                _ => false,
            },

            _ => self.follows_pattern(board, old_idx, new_idx),
        }
    }

    pub fn out_of_bounds_step(prev_idx: i8, next_idx: i8) -> bool {
        if (prev_idx % 8 == 0 && next_idx % 8 == 7) || (prev_idx % 8 == 7 && next_idx % 8 == 0) && (0..64).contains(&next_idx) {
            return true;
        }
        return false;
    }

    fn not_blocked(board: &Board, start_idx: i8, direction: i8, steps: i8) -> bool {
        let mut prev_idx = start_idx;
        for _i in 1..steps {
            let next_idx = prev_idx + direction;
            if board.board_vec[next_idx as usize].color != Colors::None
                || Piece::out_of_bounds_step(prev_idx, next_idx)
            {
                return false;
            }
            prev_idx = next_idx;
        }
        true
    }

    pub fn legal_move(&self, board: &Board, old_idx: i8, new_idx: i8) -> bool {
        if board.board_vec[old_idx as usize].color == board.board_vec[new_idx as usize].color {
            return false;
        }
        if self.follows_pattern(&board, old_idx, new_idx) {
            let mut temp_board = board.clone();
            temp_board.board_vec[new_idx as usize] = temp_board.board_vec[old_idx as usize].clone();
            temp_board.board_vec[old_idx as usize] = Piece { color: Colors::None, piece: Pieces::Empty };
            return !temp_board.calculate_check(&self.color);
        }
        false
    }
}

#[derive(PartialEq)]
pub enum GameResult {
    WhiteWin,
    BlackWin,
    Draw,
    Ongoing,
}

use std::fmt;
use std::fmt::Formatter;
use std::fmt::Error;

impl fmt::Display for GameResult {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let result = match self {
            GameResult::BlackWin => "Black win",
            GameResult::WhiteWin => "White win",
            GameResult::Draw => "Draw",
            GameResult::Ongoing => "Ongoing"
        };
        write!(f, "{}", result)
    }
}



