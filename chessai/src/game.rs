use crate::board_logic::Board;
use crate::board_logic::Colors;
use crate::board_logic::GameResult;

use crate::players::Player;

pub struct Game<T, U> {
    white_player: T,
    black_player: U,
    board: Board,
}

impl<T: Player, U: Player> Game<T, U>
{
    pub fn new(white: T, black: U) -> Game<T, U> {
        Game {
            white_player: white,
            black_player: black,
            board: Board::new(),
        }
    }

    pub fn start(&mut self) {
        print!("Initial result: {} \n", self.board.status());
        while self.board.status() == GameResult::Ongoing {
            print!("Player {}s turn to move ", self.board.color_to_move);
            let mv = match self.board.color_to_move {
                Colors::Black => self.black_player.generate_move(&self.board),
                Colors::White => self.white_player.generate_move(&self.board),
                _ => panic!()
            };
            print!("start index: {}, end index: {} \n", mv.0, mv.1);
            self.board = self.board.make_move(mv.0, mv.1);
            print!("Player {}s turn to move ", self.board.color_to_move);
            print!("Possible moves: {} ", self.board.legal_moves().len());
            print!("Checked: {} ", self.board.is_checked);

            print!("Game result: {}\n", self.board.status())
        }
    }
}
