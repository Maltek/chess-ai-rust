pub struct Random {}

struct ArtificialIntelligence {}

use crate::board_logic::Board;

pub trait Player {
    fn generate_move(&mut self, board: &Board) -> (i8, i8);
}

use rand::seq::SliceRandom;

impl Player for Random {
    fn generate_move(&mut self, board: &Board) -> (i8, i8) {
        *board.legal_moves().choose(&mut rand::thread_rng()).unwrap()
    }
}

use std::collections::VecDeque;

pub struct VitSkolmatt {
    moves: VecDeque<(i8, i8)>
}

impl VitSkolmatt {
    pub fn new() -> VitSkolmatt {
        let mut moves = VecDeque::new();
        moves.push_back((Board::sqr_to_idx("e2"), Board::sqr_to_idx("e4")));
        moves.push_back((Board::sqr_to_idx("d1"), Board::sqr_to_idx("f3")));
        moves.push_back((Board::sqr_to_idx("f1"), Board::sqr_to_idx("c4")));
        moves.push_back((Board::sqr_to_idx("f3"), Board::sqr_to_idx("f7")));

        VitSkolmatt { moves }
    }
}

impl Player for VitSkolmatt {
    fn generate_move(&mut self, _board: &Board) -> (i8, i8) {
        self.moves.pop_front().unwrap_or((0, 0))
    }
}

pub struct SvartSkolmatt {
    moves: VecDeque<(i8, i8)>
}

impl SvartSkolmatt {
    pub fn new() -> SvartSkolmatt {
        let mut moves = VecDeque::new();
        moves.push_back((Board::sqr_to_idx("e7"), Board::sqr_to_idx("e5")));
        moves.push_back((Board::sqr_to_idx("b8"), Board::sqr_to_idx("c6")));
        moves.push_back((Board::sqr_to_idx("d7"), Board::sqr_to_idx("d6")));
        SvartSkolmatt { moves }
    }
}

impl Player for SvartSkolmatt {
    fn generate_move(&mut self, _board: &Board) -> (i8, i8) {
        self.moves.pop_front().unwrap_or((0, 0))
    }
}
