use chessai::board_logic::{Board, Piece, Pieces, Colors, GameResult};

#[test]
fn square_to_index_tests() {
    assert_eq!(0, Board::sqr_to_idx("a1"));
    assert_eq!(63, Board::sqr_to_idx("h8"));
    assert_eq!(28, Board::sqr_to_idx("e4"));
    assert_eq!(1, Board::sqr_to_idx("b1"));
    assert_eq!(58, Board::sqr_to_idx("c8"));
    assert_eq!(61, Board::sqr_to_idx("f8"));
    assert_eq!(4, Board::sqr_to_idx("e1"));

    assert_eq!(Board::sqr_to_idx("b3"), Board::sqr_to_idx("b1") + 16)
}

#[test]
fn white_pawn_one_step() {
    let board = Board::new();
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("e4"))
    );
}

#[test]
fn white_pawn_two_steps() {
    let board = Board::new();
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("e4"))
    );
}

#[test]
fn white_pawn_three_steps() {
    let board = Board::new();
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("e5"))
    );
}

#[test]
fn white_pawn_take_piece() {
    let mut board = Board::new();
    board["d3"] = Piece::new(Pieces::Rook, Colors::Black);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("d3"))
    );
}

#[test]
fn white_pawn_diagonal_without_take() {
    let board = Board::new();
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("d3"))
    );
}

#[test]
fn black_pawn_one_step() {
    let mut board = Board::new();
    board.color_to_move = Colors::Black;

    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("e7"), Board::sqr_to_idx("e6"))
    );
}

#[test]
fn black_pawn_two_steps() {
    let mut board = Board::new();
    board.color_to_move = Colors::Black;

    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("e7"), Board::sqr_to_idx("e5"))
    );
}

#[test]
fn black_pawn_three_steps() {
    let board = Board::new();
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("e7"), Board::sqr_to_idx("e4"))
    );
}

#[test]
fn black_pawn_take_piece() {
    let mut board = Board::new();
    board.color_to_move = Colors::Black;

    board["d6"] = Piece::new(Pieces::Rook, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("e7"), Board::sqr_to_idx("d6"))
    );
}

#[test]
fn black_pawn_diagonal_without_take() {
    let mut board = Board::new();
    board.color_to_move = Colors::Black;

    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("e7"), Board::sqr_to_idx("d6"))
    );
}

#[test]
fn move_pawn_two_steps_not_on_start_position() {
    let mut board = Board::new();
    board["d6"] = Piece::new(Pieces::Pawn, Colors::Black);
    board["b3"] = Piece::new(Pieces::Pawn, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("b3"), Board::sqr_to_idx("b5"))
    );
    board.color_to_move = Colors::Black;
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("d6"), Board::sqr_to_idx("d4"))
    );
}

#[test]
fn move_bishop_one_diagonal_step() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Bishop, Colors::White);
    board["d5"] = Piece::new(Pieces::King, Colors::Black);
    board["d3"] = Piece::new(Pieces::King, Colors::White);

    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e5"))
    );
}

#[test]
fn move_bishop_two_diagonal_steps() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Bishop, Colors::White);
    board["f5"] = Piece::new(Pieces::Bishop, Colors::White);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("b6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("f6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("f5"), Board::sqr_to_idx("d3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("f5"), Board::sqr_to_idx("h3"))
    );
}

#[test]
fn move_bishop_when_blocked() {
    let mut board = Board::new();
    board["d4"] = Piece::new(Pieces::Bishop, Colors::White);
    board["e5"] = Piece::new(Pieces::Bishop, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("f6"))
    );
}

#[test]
fn take_opponent_with_bishop() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Bishop, Colors::White);
    board["g7"] = Piece::new(Pieces::Queen, Colors::Black);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("g7"))
    );
}

#[test]
fn take_own_piece_with_bishop() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Bishop, Colors::White);
    board["f2"] = Piece::new(Pieces::Queen, Colors::White);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("f2"))
    );
}

#[test]
fn move_all_directions_with_king() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c4"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e4"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e5"))
    );
}

#[test]
fn illegal_rook_move() {
    let mut board = Board::blank();
    board["h8"] = Piece::new(Pieces::Rook, Colors::White);
    board["a8"] = Piece::new(Pieces::Rook, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a8"), Board::sqr_to_idx("e1"))
    );

    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h8"), Board::sqr_to_idx("e1"))
    );
}

#[test]
fn not_checked_by_straight_pawn_move() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::King, Colors::White);
    board["a2"] = Piece::new(Pieces::Pawn, Colors::White);
    board["d5"] = Piece::new(Pieces::Pawn, Colors::Black);
    board["a6"] = Piece::new(Pieces::King, Colors::Black);

    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("a2"), Board::sqr_to_idx("a3"))
    );
}

#[test]
fn not_checked_at_start() {
    let board = Board::new();
    assert_eq!(false, board.is_checked);
    assert_eq!(false, board.is_checked);
}

#[test]
fn move_two_steps_with_king() {
    let mut board = Board::new();
    board["d4"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("a4"))
    );
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("h8"))
    );
}

#[test]
fn move_one_step_all_directions_with_rook() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Rook, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c4"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e4"))
    );
}

#[test]
fn move_several_steps_all_directions_with_rook() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Rook, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("a4"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e4"))
    );
}

#[test]
fn move_one_step_all_directions_with_queen() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Queen, Colors::White);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c4"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e4"))
    );
}

#[test]
fn move_several_steps_in_straight_line_with_queen() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Queen, Colors::White);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("a4"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("d5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e4"))
    );
}

#[test]
fn move_queen_one_diagonal_step() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Queen, Colors::White);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("c5"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("e5"))
    );
}

#[test]
fn move_queen_two_diagonal_steps() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Queen, Colors::White);
    board["f5"] = Piece::new(Pieces::Queen, Colors::White);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("b6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("f6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("f5"), Board::sqr_to_idx("d3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("f5"), Board::sqr_to_idx("h3"))
    );
}

#[test]
fn move_queen_when_blocked() {
    let mut board = Board::new();
    board["d4"] = Piece::new(Pieces::Queen, Colors::White);
    board["e5"] = Piece::new(Pieces::Queen, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("f6"))
    );
}

#[test]
fn take_opponent_with_queen() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Queen, Colors::White);
    board["g7"] = Piece::new(Pieces::Queen, Colors::Black);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("g7"))
    );
}

#[test]
fn take_own_piece_with_queen() {
    let mut board = Board::blank();
    board["d4"] = Piece::new(Pieces::Queen, Colors::White);
    board["f2"] = Piece::new(Pieces::Queen, Colors::White);
    board["a3"] = Piece::new(Pieces::King, Colors::Black);
    board["a5"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("d4"), Board::sqr_to_idx("f2"))
    );
}

#[test]
fn knight_up() {
    let mut board = Board::blank();
    board["b1"] = Piece::new(Pieces::Knight, Colors::White);
    board["g1"] = Piece::new(Pieces::Knight, Colors::White);

    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("b1"), Board::sqr_to_idx("c3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("b1"), Board::sqr_to_idx("a3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("g1"), Board::sqr_to_idx("f3"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("g1"), Board::sqr_to_idx("h3"))
    );
}

#[test]
fn knight_down() {
    let mut board = Board::new();
    board.color_to_move = Colors::Black;
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("b8"), Board::sqr_to_idx("c6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("b8"), Board::sqr_to_idx("a6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("g8"), Board::sqr_to_idx("f6"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("g8"), Board::sqr_to_idx("h6"))
    );
}

#[test]
fn king_go_down_left_from_column_a() {
    let mut board = Board::new();
    board["a4"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a4"), Board::sqr_to_idx("h3"))
    );
}

#[test]
fn bishop_go_down_left_from_column_a() {
    let mut board = Board::new();
    board["a5"] = Piece::new(Pieces::Bishop, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a5"), Board::sqr_to_idx("h3"))
    );
}

#[test]
fn bishop_go_up_left_from_column_a() {
    let mut board = Board::new();
    board["a5"] = Piece::new(Pieces::Bishop, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a5"), Board::sqr_to_idx("h5"))
    );
}

#[test]
fn queen_go_down_left_from_column_a() {
    let mut board = Board::new();
    board["a5"] = Piece::new(Pieces::Queen, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a5"), Board::sqr_to_idx("h3"))
    );
}

#[test]
fn knight_go_left_past_column_a() {
    let mut board = Board::new();
    board["a6"] = Piece::new(Pieces::Knight, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a6"), Board::sqr_to_idx("h7"))
    );
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a6"), Board::sqr_to_idx("h3"))
    );
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a6"), Board::sqr_to_idx("f6"))
    );
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a6"), Board::sqr_to_idx("g4"))
    );
}

#[test]
fn king_go_right_from_column_h() {
    let mut board = Board::new();
    board["h6"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h6"), Board::sqr_to_idx("a7"))
    )
}

#[test]
fn bishop_go_down_right_from_column_h() {
    let mut board = Board::new();
    board["h5"] = Piece::new(Pieces::Bishop, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h5"), Board::sqr_to_idx("a7"))
    );
}

#[test]
fn bishop_go_up_right_from_column_h() {
    let mut board = Board::new();
    board["h5"] = Piece::new(Pieces::Bishop, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h5"), Board::sqr_to_idx("a5"))
    );
}

#[test]
fn knight_go_right_past_column_a() {
    let mut board = Board::new();
    board["h5"] = Piece::new(Pieces::Knight, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h5"), Board::sqr_to_idx("a7"))
    );
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h5"), Board::sqr_to_idx("a3"))
    );
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h5"), Board::sqr_to_idx("b5"))
    );
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h5"), Board::sqr_to_idx("b7"))
    );
}

#[test]
fn pawn_take_past_column_a() {
    let mut board = Board::new();
    board["a7"] = Piece::new(Pieces::Pawn, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a7"), Board::sqr_to_idx("h7"))
    );
}

#[test]
fn pawn_take_past_column_h() {
    let mut board = Board::new();
    board["h6"] = Piece::new(Pieces::Pawn, Colors::White);
    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("h6"), Board::sqr_to_idx("a8"))
    );
}

#[test]
fn basic_is_checked() {
    let mut board = Board::blank();
    board["e3"] = Piece::new(Pieces::King, Colors::White);
    board["e5"] = Piece::new(Pieces::Rook, Colors::Black);
    assert_eq!(true, board.calculate_check(&Colors::White));
}

#[test]
fn basic_false_is_checked() {
    let mut board = Board::blank();
    board["e3"] = Piece::new(Pieces::King, Colors::White);
    assert_eq!(false, board.calculate_check(&Colors::White));
}

#[test]
fn must_stop_being_checked() {
    let mut board = Board::blank();
    board["e3"] = Piece::new(Pieces::King, Colors::White);
    board["a3"] = Piece::new(Pieces::Pawn, Colors::White);
    board["e5"] = Piece::new(Pieces::Rook, Colors::Black);

    assert_eq!(
        false,
        board.legal_move(Board::sqr_to_idx("a3"), Board::sqr_to_idx("a4"))
    );
    assert_eq!(
        true,
        board.legal_move(Board::sqr_to_idx("e3"), Board::sqr_to_idx("d3"))
    );
}

#[test]
fn move_piece() {
    let mut board = Board::blank();
    board["e2"] = Piece::new(Pieces::Pawn, Colors::White);
    let new_board = board.make_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("e4"));
    let mut board2 = Board::blank();
    board2["e4"] = Piece::new(Pieces::Pawn, Colors::White);
    assert!(
        new_board.board_vec.iter().eq(board2.board_vec.iter()),
        "Arrays are not equal"
    );
}

#[test]
fn legal_moves_with_only_pawn() {
    let mut board = Board::blank();
    board["e3"] = Piece::new(Pieces::Pawn, Colors::White);
    assert!(board.legal_moves() == vec!((20, 28)));
}

#[test]
fn legal_moves_with_blocked_pawn() {
    let mut board = Board::blank();
    board["e3"] = Piece::new(Pieces::Pawn, Colors::White);
    board["e4"] = Piece::new(Pieces::Pawn, Colors::Black);

    assert!(board.legal_moves() == Vec::new());
}

#[test]
fn no_legal_moves_for_black() {
    let mut board = Board::blank();
    board.color_to_move = Colors::Black;
    board["e3"] = Piece::new(Pieces::Pawn, Colors::White);

    assert!(board.legal_moves() == Vec::new());
}

#[test]
fn no_king_has_moved_at_start() {
    let board = Board::new();
    assert!(!board.w_k_has_moved);
    assert!(!board.b_k_has_moved);
}

#[test]
fn no_king_has_moved_with_blank_board() {
    let board = Board::blank();
    assert!(!board.w_k_has_moved);
    assert!(!board.b_k_has_moved);
}

#[test]
fn moving_king_changes_value() {
    let board = Board::new();
    let board = board.make_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("e4"));
    let board = board.make_move(Board::sqr_to_idx("e3"), Board::sqr_to_idx("e5"));
    let board = board.make_move(Board::sqr_to_idx("e1"), Board::sqr_to_idx("e2"));
    let board = board.make_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("e7"));

    assert!(board.w_k_has_moved);
    assert!(board.b_k_has_moved);
}

#[test]
fn white_can_castle() {
    let mut board = Board::blank();
    board["e1"] = Piece::new(Pieces::King, Colors::White);
    board["h1"] = Piece::new(Pieces::Rook, Colors::White);
    board["a1"] = Piece::new(Pieces::Rook, Colors::White);
    board["e8"] = Piece::new(Pieces::King, Colors::Black);


    assert!(board.legal_move(Board::sqr_to_idx("e1"), Board::sqr_to_idx("g1")));
    assert!(board.legal_move(Board::sqr_to_idx("e1"), Board::sqr_to_idx("c1")));
}

#[test]
fn black_can_castle() {
    let mut board = Board::blank();
    board.color_to_move = Colors::Black;
    board["e8"] = Piece::new(Pieces::King, Colors::Black);
    board["h8"] = Piece::new(Pieces::Rook, Colors::Black);
    board["a8"] = Piece::new(Pieces::Rook, Colors::Black);
    board["e1"] = Piece::new(Pieces::King, Colors::White);


    assert!(board.legal_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("g8")));
    assert!(board.legal_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("c8")));
}

#[test]
fn cant_castle_when_checked() {
    let mut board = Board::blank();
    board.color_to_move = Colors::Black;
    board["e8"] = Piece::new(Pieces::King, Colors::Black);
    board["h8"] = Piece::new(Pieces::Rook, Colors::Black);
    board["e1"] = Piece::new(Pieces::Rook, Colors::White);

    assert!(!board.legal_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("g8")));
}

#[test]
fn cant_castle_over_check() {
    let mut board = Board::blank();
    board.color_to_move = Colors::Black;
    board["e8"] = Piece::new(Pieces::King, Colors::Black);
    board["h8"] = Piece::new(Pieces::Rook, Colors::Black);
    board["f1"] = Piece::new(Pieces::Rook, Colors::White);

    assert!(!board.legal_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("g8")));
}

#[test]
fn cant_castle_when_king_has_moved() {
    let mut board = Board::blank();
    board["e8"] = Piece::new(Pieces::King, Colors::Black);
    board["h8"] = Piece::new(Pieces::Rook, Colors::Black);
    board.color_to_move = Colors::Black;
    board = board.make_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("e7"));
    board.color_to_move = Colors::Black;
    board = board.make_move(Board::sqr_to_idx("e7"), Board::sqr_to_idx("e8"));
    board.color_to_move = Colors::Black;
    assert!(!board.legal_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("g8")));
}

#[test]
fn cant_castle_when_rook_has_moved() {
    let mut board = Board::blank();
    board["e8"] = Piece::new(Pieces::King, Colors::Black);
    board["h8"] = Piece::new(Pieces::Rook, Colors::Black);
    board.color_to_move = Colors::Black;
    board = board.make_move(Board::sqr_to_idx("h8"), Board::sqr_to_idx("h7"));
    board.color_to_move = Colors::Black;
    board = board.make_move(Board::sqr_to_idx("h7"), Board::sqr_to_idx("h8"));
    board.color_to_move = Colors::Black;
    assert!(!board.legal_move(Board::sqr_to_idx("e8"), Board::sqr_to_idx("g8")));
}

#[test]
fn moving_changes_color_to_move() {
    let mut board = Board::new();
    board = board.make_move(Board::sqr_to_idx("e2"), Board::sqr_to_idx("e4"));
    assert!(board.color_to_move == Colors::Black);
}

#[test]
fn white_starts_moving() {
    let board = Board::new();
    assert!(board.color_to_move == Colors::White);
}


#[test]
fn game_is_ongoing_at_start() {
    let mut board = Board::new();
    board.is_checked = board.calculate_check(&board.color_to_move);
    assert!(GameResult::Ongoing == board.status());
}

#[test]
fn white_can_win() {
    let mut board = Board::blank();
    board["a8"] = Piece::new(Pieces::King, Colors::Black);
    board["a7"] = Piece::new(Pieces::Queen, Colors::White);
    board["a6"] = Piece::new(Pieces::King, Colors::White);
    board.color_to_move = Colors::Black;
    board.is_checked = board.calculate_check(&board.color_to_move);
    assert!(GameResult::WhiteWin == board.status());
}


#[test]
fn black_can_win() {
    let mut board = Board::blank();
    board["a8"] = Piece::new(Pieces::King, Colors::White);
    board["a7"] = Piece::new(Pieces::Queen, Colors::Black);
    board["a6"] = Piece::new(Pieces::King, Colors::Black);
    board.is_checked = board.calculate_check(&Colors::White);
    assert!(GameResult::BlackWin == board.status());
}

#[test]
fn game_can_draw() {
    let mut board = Board::blank();
    board["a8"] = Piece::new(Pieces::King, Colors::White);
    board["c7"] = Piece::new(Pieces::Queen, Colors::Black);
    board["a6"] = Piece::new(Pieces::King, Colors::Black);
    board.is_checked = board.calculate_check(&Colors::White);

    assert!(GameResult::Draw == board.status());
}

#[test]
fn win_when_nonking_move_is_possible() {
    let mut board = Board::blank();
    board["a8"] = Piece::new(Pieces::King, Colors::Black);
    board["e8"] = Piece::new(Pieces::Rook, Colors::Black);
    board["a7"] = Piece::new(Pieces::Queen, Colors::White);
    board["a6"] = Piece::new(Pieces::King, Colors::White);
    board.color_to_move = Colors::Black;
    board.is_checked = board.calculate_check(&Colors::Black);

    assert!(GameResult::WhiteWin == board.status());
}

#[test]
fn correct_amount_of_legal_moves_at_start() {
    let board = Board::new();
    assert_eq!(20, board.legal_moves().len())
}


#[test]
fn correct_legal_moves_with_bishop() {
    let mut board = Board::blank();
    board["a8"] = Piece::new(Pieces::Bishop, Colors::White);
    let possible_moves = vec!((Board::sqr_to_idx("a8"), Board::sqr_to_idx("h1")),
                              (Board::sqr_to_idx("a8"), Board::sqr_to_idx("g2")),
                              (Board::sqr_to_idx("a8"), Board::sqr_to_idx("f3")),
                              (Board::sqr_to_idx("a8"), Board::sqr_to_idx("e4")),
                              (Board::sqr_to_idx("a8"), Board::sqr_to_idx("d5")),
                              (Board::sqr_to_idx("a8"), Board::sqr_to_idx("c6")),
                              (Board::sqr_to_idx("a8"), Board::sqr_to_idx("b7")));
    let legal_moves = board.legal_moves();
    assert_eq!(legal_moves, possible_moves);
}

#[test]
fn out_of_bounds_test() {
    assert!(Piece::out_of_bounds_step(7, 0))
}

use chessai::players;
use chessai::game::Game;
#[test]
fn scholars_mate() {
    let white = players::VitSkolmatt::new();
    let black = players::SvartSkolmatt::new();
    let mut game = Game::new(white, black);
    game.start();
}