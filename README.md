# chess-ai-rust
A project to learn rust by developing a simple AI that can play chess. The goals are:
* A graphical user interface to interact easily with the AI.
* An implementation of some kind of alpha-beta pruning algorithm for the AI to use.
* By experience learn how to use rust and how to follow the coding conventions in the language.


# Design of the program
The first task is to figure out how to design the system. To start with:
* The GUI is it's own class
* The AI should be it's own class. It should implement a function to calculate rewards and choose the one with highest expected value.
* Players should interact with the game through a game class that is the sole owner of board belonging to the game.
* There should be a class to handle the board logic. This class keeps track of the board state. I might also implement some kind of "legal moves" function in this class to limit the player choices of how to move and not make the AI figure out where it can go.

# The board class
The board class needs to handle the state of the game and preliminarily it keeps track of the legal moves as well, but this might be refactored out to a separate class to keep class roles distinct.
In chess there are many relevant rules to keep track of except for the current positions on the board. On the top of my head I can name several features needing to be tracked:
* How many times the current position has been repeated with the same player to move. If it happens three times we have a draw.
* If the king or rook has moved earlier to prevent castling illegally.
* How many moves it has been since the last piece was taken or a pawn was moved. If this has happened a draw occurs.
* If a pawn took two steps the previous turn for en passant

Things might be appended here in the future depending on my level of ambition and how much I care about following current FIDE rules.

In chess there are 6 different kinds of pieces that can be of two different colors placed on basically any place on the board. We need to keep track on every one of these positions and also the separate rules for where these can go. It might be a good idea to use some kind of enum to keep track of the pieces and what color they are. I now have a rough idea of this class needs to do and will present a preliminary list for how to do it.
* A matrix modeled in some way to keep track of which tile on the board has which piece on it. This structure should have low overhead and a very simple structure. The entries should probable be a number for the piece and some number to symbolize the player, or possibly simply an enum with separate numbers for let's say black and white rooks.
* The aforementioned piece enums
* The logic for how the pieces move will move
* Logic to determine end states i.e. draws, one player has won.
* A way to list possible moves for a given player

# Ideas that need to be implemented
- General refactoring
  -follow_pattern
    - follows pattern should break out some kind of is blocked function
    - Maybe break out piece patterns for some kind trait on them for easier to read code